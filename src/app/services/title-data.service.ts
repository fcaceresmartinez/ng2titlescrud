import { Injectable } from '@angular/core';
import { Title } from '../title';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class TitleDataService {

  lastId: number = 0;

  titles: Title[] = [];
  // API endpoint
  private BASE_URL: string = 'https://dzt9ceh093.execute-api.us-east-1.amazonaws.com/dev/movie/';

  constructor(private http: Http) {

  }

  // POST
  public addTitle(title: Title) {
    let options = new RequestOptions({
      headers: new Headers({'Content-Type': 'application/json;charset=utf-8'})
    });
    return this.http.post(`${this.BASE_URL}`, JSON.stringify(title), options).map((res: Response) => res.json())
    .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  // DELETE
  public deleteTitleById(id: number) {
    let options = new RequestOptions({
      headers: new Headers({ 'Content-Type': 'application/json;charset=UTF-8' })
    });

  return this.http.delete(`${this.BASE_URL}${id}`, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // GET by Id
  public getTitleById(id: number) {
    let options = new RequestOptions({
      headers: new Headers({ 'Content-Type': 'application/json;charset=UTF-8' })
    });

  return this.http.get(`${this.BASE_URL}${id}`, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // LIST
  public getTitles() {
    return this.http.get(`${this.BASE_URL}`).map((res: Response) => res.json())
    .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  // PUT
  public updateTitle(title: Title) {
    let options = new RequestOptions({
      headers: new Headers({'Content-Type': 'application/json;charset=utf-8'})
    });
    return this.http.put(`${this.BASE_URL}${title['id']}`, JSON.stringify(title), options).map((res: Response) => res.json())
    .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }


}
