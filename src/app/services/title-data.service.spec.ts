/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TitleDataService } from './title-data.service';
import { Title } from '../title';

describe('TitleDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TitleDataService]
    });
  });

  it('should ...', inject([TitleDataService], (service: TitleDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getTitles()', () => {
    it('should return an empty array by default', inject([TitleDataService], (service: TitleDataService) => {
      expect(service.getTitles()).toEqual([]);
    }));

    it('should return all titles', inject([TitleDataService], (service: TitleDataService) => {
      let title1 = new Title({id: 1, title: 'Why Him', description: 'Why this movie', cast: ['James Franco', 'Bryan Cranston']});
      let title2 = new Title({id: 2, title: 'Doctor Strange', description: 'Herr Doktor', cast: ['Benedict Cumberbatch', 'Tilda Swinton']});
      service.addTitle(title1);
      service.addTitle(title2);
      expect(service.getTitles()).toEqual([title1, title2]);
    }));
  });

  describe('#save(title)', () => {
    it('should automatically assign an incremental id', inject([TitleDataService], (service: TitleDataService) => {
      let title1 = new Title({id: 1, title: 'Why Him', description: 'Why this movie', cast: ['James Franco', 'Bryan Cranston']});
      let title2 = new Title({id: 2, title: 'Doctor Strange', description: 'Herr Doktor', cast: ['Benedict Cumberbatch', 'Tilda Swinton']});
      service.addTitle(title1);
      service.addTitle(title2);
      expect(service.getTitleById(1)).toEqual(title1);
      expect(service.getTitleById(2)).toEqual(title2);
    }));
  });

  describe('#deleteTitleById()', () => {
    it('should remove title with the correspondent id', inject([TitleDataService], (service: TitleDataService) => {
      let title1 = new Title({id: 1, title: 'Why Him', description: 'Why this movie', cast: ['James Franco', 'Bryan Cranston']});
      let title2 = new Title({id: 2, title: 'Doctor Strange', description: 'Herr Doktor', cast: ['Benedict Cumberbatch', 'Tilda Swinton']});
      service.addTitle(title1);
      service.addTitle(title2);
      expect(service.getTitles()).toEqual([title1, title2]);
      service.deleteTitleById(1);
      expect(service.getTitles()).toEqual([title2]);
      service.deleteTitleById(2);
      expect(service.getTitles()).toEqual([]);
    }));

    it('should not remove anything if id is not found', inject([TitleDataService], (service: TitleDataService) => {
      let title1 = new Title({id: 1, title: 'Why Him', description: 'Why this movie', cast: ['James Franco', 'Bryan Cranston']});
      let title2 = new Title({id: 2, title: 'Doctor Strange', description: 'Herr Doktor', cast: ['Benedict Cumberbatch', 'Tilda Swinton']});
      service.addTitle(title1);
      service.addTitle(title2);
      expect(service.getTitles()).toEqual([title1, title2]);
      service.deleteTitleById(3);
      expect(service.getTitles()).toEqual([title1, title2]);
    }));
  });

 });
