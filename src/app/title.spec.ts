import {Title} from './title';

describe('Title', () => {
  it('should create an instance', () => {
    expect(new Title()).toBeTruthy();
  });

  it('should accept values in the constructor', () => {
    let title = new Title({
      id: 1,
      title: 'Rogue One',
      description: 'A Star Wars Story',
      cast: [
        'Felicity Jones',
        'Diego Luna'
      ]
    });
    expect(title.id).toEqual(1);
    expect(title.name).toEqual('Rogue One');
    expect(title.description).toEqual('A Star Wars Story');
    expect(title.cast).toEqual(['Felicity Jones', 'Diego Luna']);
  });
});
