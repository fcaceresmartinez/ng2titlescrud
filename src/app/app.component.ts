import { Component, OnInit, Input } from '@angular/core';
import { TitleDataService } from './services/title-data.service';
import { EmitterService } from './services/emitter.service';
import { Title } from './title';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { KeysPipe } from './pipes/keys.pipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TitleDataService, EmitterService]
})
export class AppComponent implements OnInit {

  public title: string = 'Poc App for Invoking AWS APIs';
  private titleInfo;
  private reset;
  private resetList;
  @Input() newTitle: Title = new Title();
  @Input() titles: Observable<Title[]>;

  constructor(private titleDataService: TitleDataService, private emitterService: EmitterService) {
  }

  ngOnInit() {
    this.titleDataService.getTitles().subscribe(response => this.titles = response.payload.Items, error => {alert(`Can't get titles'`); });
  }

  public addTitle() {
    this.titleDataService.addTitle(this.newTitle).subscribe(response => this.titles = response.payload.Items, error =>
    {alert('Unable to add user')});
    this.newTitle = new Title();
  }

  removeTitle(title) {
    this.titleDataService.deleteTitleById(title.id);
  }

  getTitleById(id: number) {
    return this.titleDataService.getTitleById(id);
  }

}
