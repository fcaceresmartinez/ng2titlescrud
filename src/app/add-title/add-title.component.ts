import { Component, OnChanges } from '@angular/core';
import {TitleDataService} from '../services/title-data.service';
import {Title} from '../title';
@Component({
  selector: 'app-add-title',
  templateUrl: './add-title.component.html',
  styleUrls: ['./add-title.component.css'],
  providers: [TitleDataService]
})
export class AddTitleComponent {

  private title: Title  = new Title();

  constructor(private titleDataService: TitleDataService) { }

}
