import { Ng2titlescrudPage } from './app.po';

describe('ng2titlescrud App', function() {
  let page: Ng2titlescrudPage;

  beforeEach(() => {
    page = new Ng2titlescrudPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
